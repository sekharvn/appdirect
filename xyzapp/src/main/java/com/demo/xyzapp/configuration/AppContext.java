package com.demo.xyzapp.configuration;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AppContext {
	
	public static AnnotationConfigApplicationContext getAppContext() {
		return new AnnotationConfigApplicationContext(AppConfig.class);
	}
}
