package com.demo.xyzapp.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

import org.apache.log4j.Logger;

import com.demo.xyzapp.model.Subscription;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SubscriptionClient {

	final static Logger logger = Logger.getLogger(SubscriptionClient.class);

	private static final String CONSUMER_SECRET = "LJoANYdJCQDn68z4";
	private static final String CONSUMER_KEY = "app-xyz-137069";

	public static Subscription getSubscription(String urlString) {
		String subscriptionResponseJson = null;
		try {
			subscriptionResponseJson = getRestResponseFromAppDirect(urlString);
		} catch (OAuthMessageSignerException e) {
			logger.error("OAuthMessageSignerException occured" + e);
		} catch (OAuthExpectationFailedException e) {
			logger.error("OAuthExpectationFailedException occured" + e);
		} catch (OAuthCommunicationException e) {
			logger.error("OAuthCommunicationException occured" + e);
		} catch (IOException e) {
			logger.error("IOException occured" + e);
		}
		
		try {
			return mapSubscriptionJsonWithObject(subscriptionResponseJson);
		} catch (JsonParseException e) {
			logger.error("JsonParseException occured" + e);
		} catch (JsonMappingException e) {
			logger.error("JsonMappingException occured" + e);
		} catch (IOException e) {
			logger.error("IOException occured" + e);
		}

		return null;
	}

	public static String getRestResponseFromAppDirect(String urlString) throws OAuthMessageSignerException,
			OAuthExpectationFailedException, OAuthCommunicationException, IOException {
		if (urlString == null) {
			return null;
		} else {
			// Signing the request with OAUTH
			OAuthConsumer consumer = new DefaultOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
			URL url = new URL(urlString);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			consumer.sign(conn);
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			StringBuffer output = new StringBuffer();
			String line;
			while ((line = br.readLine()) != null) {
				output.append(line);
			}
			conn.disconnect();
			return output.toString();
		}
	}

	public static Subscription mapSubscriptionJsonWithObject(String subscriptionJson) throws JsonParseException,
			JsonMappingException, IOException {
		if (subscriptionJson == null) {
			return null;
		} else {
			ObjectMapper objectMapper = new ObjectMapper();
			return objectMapper.readValue(subscriptionJson, Subscription.class);
		}
	}

}
