package com.demo.xyzapp.business;

import java.util.List;

import org.springframework.stereotype.Service;

import com.db.MockDB;
import com.demo.xyzapp.model.Contact;

@Service("contactBusinessService")
public class ContactBusinessService implements IContactBusinessService {

	public List<Contact> getAllContacts() {
		return MockDB.getInstance().getAllContacts();
	}

	public Contact getContactByName(String name) {
		return MockDB.getInstance().getContactByName(name);
	}

	public Contact updateContact(Contact contact) {
		return MockDB.getInstance().updateContact(contact);
	}

	public boolean deleteContact(String name) {
		return MockDB.getInstance().deleteContact(name);
	}

	public Contact createContact(Contact contact) {
		return MockDB.getInstance().insertContact(contact);
	}

}
