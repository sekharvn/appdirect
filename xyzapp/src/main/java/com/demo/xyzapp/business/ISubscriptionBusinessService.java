package com.demo.xyzapp.business;

import com.demo.xyzapp.model.NotificationResponse;
import com.demo.xyzapp.model.Subscription;

public interface ISubscriptionBusinessService {

	NotificationResponse processCreateSubscription(Subscription subscriptionOrder);
	
	NotificationResponse processCancelSubscription(Subscription subscriptionCancel);
	
	NotificationResponse processChangeSubscription(Subscription subscriptionChange);
	
	NotificationResponse processSubscriptionStatusChange(Subscription subscriptionNotice);
}
