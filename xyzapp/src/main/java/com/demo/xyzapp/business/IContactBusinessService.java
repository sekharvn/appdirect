package com.demo.xyzapp.business;

import java.util.List;

import com.demo.xyzapp.model.Contact;

public interface IContactBusinessService {

	List<Contact> getAllContacts();

	Contact getContactByName(String name);

	Contact updateContact(Contact contact);

	boolean deleteContact(String name);

	Contact createContact(Contact contact);
}
