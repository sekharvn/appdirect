package com.demo.xyzapp.business;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.xyzapp.dao.ISubscriptionDao;
import com.demo.xyzapp.model.NotificationResponse;
import com.demo.xyzapp.model.Subscription;
import com.demo.xyzapp.utility.SubscriptionClient;

@Service("subscriptionBusinessService")
public class SubscriptionBusinessService implements ISubscriptionBusinessService {

	final static Logger logger = Logger.getLogger(SubscriptionClient.class);

	private ISubscriptionDao subscriptionDao;

	public NotificationResponse processCreateSubscription(Subscription subscriptionOrder) {
		try {
			String accId = subscriptionDao.createSubscription(subscriptionOrder);
			if (accId != null) {
				return new NotificationResponse(true, "Success: creation of subscription", accId, null);
			}
			return new NotificationResponse(false, "Failure: creation of subscription", null, "Errorcode");
		} catch (Exception e) {
			logger.error("Exception occured while processing create subscription");
			return new NotificationResponse(false, "Exception occured while updating creation of subscription: "
					+ e.getMessage(), null, "Errorcode");
		}

	}

	public NotificationResponse processCancelSubscription(Subscription subscriptionCancel) {
		try {
			String accId = subscriptionDao.cancelSubscription(subscriptionCancel);
			if (accId != null) {
				return new NotificationResponse(true, "Success: Updation of cancellation of subscription", accId, null);
			} else {
				return new NotificationResponse(false, "Failure: Updation of cancellation of subscription", null,
						"Errorcode");
			}
		} catch (Exception e) {
			logger.error("Exception occured while processing cancel subscription");
			return new NotificationResponse(false, "Exception occured while updating cancelling of subscription: "
					+ e.getMessage(), null, "Errorcode");
		}
	}

	public NotificationResponse processChangeSubscription(Subscription subscriptionChange) {
		try {
			String accId = subscriptionDao.changeSubscription(subscriptionChange);
			if (accId != null) {
				return new NotificationResponse(true, "Success: Updation of change of subscription", accId, null);
			}
			return new NotificationResponse(false, "Failure: Updation of change of subscription", null, "Errorcode");
		} catch (Exception e) {
			logger.error("Exception occured while processing change subscription");
			return new NotificationResponse(false, "Exception occured while updating changing of subscription: "
					+ e.getMessage(), null, "Errorcode");
		}
	}

	public NotificationResponse processSubscriptionStatusChange(Subscription subscriptionNotice) {
		try {
			String accId = subscriptionDao.subscriptionStatusChange(subscriptionNotice);
			if (accId != null) {
				return new NotificationResponse(true, "Success: Updation of status change of subscription", accId, null);
			}
			return new NotificationResponse(false, "Failure: Updation of status change of subscription", null,
					"Errorcode");
		} catch (Exception e) {
			logger.error("Exception occured while processing subscription status change");
			return new NotificationResponse(false, "Exception occured while updating status change of subscription: "
					+ e.getMessage(), null, "Errorcode");
		}
	}

	public ISubscriptionDao getSubscriptionDao() {
		return subscriptionDao;
	}

	@Autowired
	public void setSubscriptionDao(ISubscriptionDao subscriptionDao) {
		this.subscriptionDao = subscriptionDao;
	}

}
