package com.demo.xyzapp.model;

/*
 * Copyright (c) 2016 GE Healthcare. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * GE Healthcare. The software may be used and/or copied only
 * with the written permission of GE Healthcare or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

/**
 * @author sekharv
 *
 */
public class Account {

	private String accountIdentifier;
	private String status;
	private String parentAccountIdentifier;

	public String getAccountIdentifier() {
		return this.accountIdentifier;
	}

	public void setAccountIdentifier(String accountIdentifier) {
		this.accountIdentifier = accountIdentifier;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getParentAccountIdentifier() {
		return parentAccountIdentifier;
	}

	public void setParentAccountIdentifier(String parentAccountIdentifier) {
		this.parentAccountIdentifier = parentAccountIdentifier;
	}

}
