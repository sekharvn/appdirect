package com.demo.xyzapp.model;

/*
 * Copyright (c) 2016 GE Healthcare. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * GE Healthcare. The software may be used and/or copied only
 * with the written permission of GE Healthcare or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

/**
 * @author sekharv
 *
 */
public class Order {

	private String editionCode;
	private String pricingDuration;
	private Item item;
	private String addonOfferingCode;
	private String[] items;

	public String getEditionCode() {
		return this.editionCode;
	}

	public void setEditionCode(String editionCode) {
		this.editionCode = editionCode;
	}

	public String getPricingDuration() {
		return this.pricingDuration;
	}

	public void setPricingDuration(String pricingDuration) {
		this.pricingDuration = pricingDuration;
	}

	public Item getItem() {
		return this.item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public String getAddonOfferingCode() {
		return addonOfferingCode;
	}

	public void setAddonOfferingCode(String addonOfferingCode) {
		this.addonOfferingCode = addonOfferingCode;
	}

	public String[] getItems() {
		return items;
	}

	public void setItems(String[] items) {
		this.items = items;
	}

}
