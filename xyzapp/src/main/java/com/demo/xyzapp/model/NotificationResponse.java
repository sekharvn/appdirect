package com.demo.xyzapp.model;

public class NotificationResponse {

	private boolean success;
	private String accountIdentifier;
	private String message;
	private String errorCode;

	public NotificationResponse() {

	}

	public NotificationResponse(boolean success, String message, String accountIdentifier, String errorCode) {
		this.success = success;
		this.message = message;
		this.accountIdentifier = accountIdentifier;
		this.errorCode = errorCode;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getAccountIdentifier() {
		return accountIdentifier;
	}

	public void setAccountIdentifier(String accountIdentifier) {
		this.accountIdentifier = accountIdentifier;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

}
