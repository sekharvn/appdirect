package com.demo.xyzapp.model;
/*
 * Copyright (c) 2016 GE Healthcare. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * GE Healthcare. The software may be used and/or copied only
 * with the written permission of GE Healthcare or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

/**
 * @author sekharv
 *
 */
public class Marketplace
{

    private String baseUrl;
    private String partner;

    public String getBaseUrl()
    {
        return this.baseUrl;
    }

    public void setBaseUrl(String baseUrl)
    {
        this.baseUrl = baseUrl;
    }

    public String getPartner()
    {
        return this.partner;
    }

    public void setPartner(String partner)
    {
        this.partner = partner;
    }

}
