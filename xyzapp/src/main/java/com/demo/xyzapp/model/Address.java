package com.demo.xyzapp.model;
/*
 * Copyright (c) 2016 GE Healthcare. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * GE Healthcare. The software may be used and/or copied only
 * with the written permission of GE Healthcare or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

/**
 * @author sekharv
 *
 */
public class Address
{
    private String firstName;
    private String fullName;
    private String lastName;
    private String city;
    private String country;
    private String phone;
    private String state;
    private String street1;
    private String zip;

    public String getFirstName()
    {
        return this.firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getFullName()
    {
        return this.fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getLastName()
    {
        return this.lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getCity()
    {
        return this.city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getCountry()
    {
        return this.country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getPhone()
    {
        return this.phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getState()
    {
        return this.state;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public String getStreet1()
    {
        return this.street1;
    }

    public void setStreet1(String street1)
    {
        this.street1 = street1;
    }

    public String getZip()
    {
        return this.zip;
    }

    public void setZip(String zip)
    {
        this.zip = zip;
    }

}
