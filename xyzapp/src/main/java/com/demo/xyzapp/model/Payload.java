package com.demo.xyzapp.model;

/*
 * Copyright (c) 2016 GE Healthcare. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * GE Healthcare. The software may be used and/or copied only
 * with the written permission of GE Healthcare or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 */

/**
 * @author sekharv
 *
 */
public class Payload {
	private Company company;
	private Order order;
	private Account account;
	private Notice notice;
	private String user;
	private String addonInstance;
	private String addonBinding;
	private Configuration configuration;

	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Order getOrder() {
		return this.order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Account getAccount() {
		return this.account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Notice getNotice() {
		return this.notice;
	}

	public void setNotice(Notice notice) {
		this.notice = notice;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getAddonInstance() {
		return addonInstance;
	}

	public void setAddonInstance(String addonInstance) {
		this.addonInstance = addonInstance;
	}

	public String getAddonBinding() {
		return addonBinding;
	}

	public void setAddonBinding(String addonBinding) {
		this.addonBinding = addonBinding;
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

}
