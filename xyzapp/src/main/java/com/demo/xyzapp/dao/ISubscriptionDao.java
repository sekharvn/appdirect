package com.demo.xyzapp.dao;

import com.demo.xyzapp.model.Subscription;

public interface ISubscriptionDao {

	String createSubscription(Subscription subscriptionOrder) throws Exception;
	
	String cancelSubscription(Subscription subscriptionCancel) throws Exception;

	String changeSubscription(Subscription subscriptionChange) throws Exception;

	String subscriptionStatusChange(Subscription subscriptionNotice) throws Exception;
	
}
