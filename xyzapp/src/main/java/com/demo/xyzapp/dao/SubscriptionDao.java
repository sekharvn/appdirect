package com.demo.xyzapp.dao;

import org.springframework.stereotype.Repository;

import com.demo.xyzapp.model.Subscription;

@Repository("subscriptionDao")
public class SubscriptionDao implements ISubscriptionDao {

	public String createSubscription(Subscription subscriptionOrder) throws Exception{
		/*
		 * Interact with database and return a Subscription id if successful, else return
		 * null. Returning a hardcoded value for now.
		 */
		return "acc123";
	}

	public String cancelSubscription(Subscription subscriptionCancel) throws Exception{
		/*
		 * Interact with database and return a Subscription id if successful, else return
		 * null. Returning a hardcoded value for now.
		 */
		return "acc159";
	}

	public String changeSubscription(Subscription subscriptionChange) throws Exception{
		/*
		 * Interact with database and return a Subscription id if successful, else return
		 * null. Returning a hardcoded value for now.
		 */
		return "acc456";
	}

	public String subscriptionStatusChange(Subscription subscriptionNotice) throws Exception{
		/*
		 * Interact with database and return a Subscription id if successful, else return
		 * null. Returning a hardcoded value for now.
		 */
		return "acc789";
	}

}
