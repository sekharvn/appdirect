package com.demo.xyzapp.service;

import com.demo.xyzapp.business.ISubscriptionBusinessService;
import com.demo.xyzapp.business.SubscriptionBusinessService;
import com.demo.xyzapp.configuration.AppContext;
import com.demo.xyzapp.model.NotificationResponse;
import com.demo.xyzapp.model.Subscription;
import com.demo.xyzapp.utility.SubscriptionClient;

/**
 * This class provides the endpoint implementations for the subscription events
 * from AppDirect
 * 
 * @author Sekhar
 *
 */
public class SubscriptionService implements ISubscriptionService {

	private ISubscriptionBusinessService subscriptionBusinessService = AppContext.getAppContext().getBean(
			SubscriptionBusinessService.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.demo.xyzapp.service.ISubscriptionService#createSubscription(java.
	 * lang.String)
	 */
	public NotificationResponse createSubscription(String eventUrl) {

		// Get more information about the new subscription, by making a REST
		// call to AppDirect
		Subscription subscriptionOrder = SubscriptionClient.getSubscription(eventUrl);

		// Perform the necessary operation in my application using the
		// subscription information
		return subscriptionBusinessService.processCreateSubscription(subscriptionOrder);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.demo.xyzapp.service.ISubscriptionService#cancelSubscription(java.
	 * lang.String)
	 */
	public NotificationResponse cancelSubscription(String eventUrl) {

		// Get more information about the cancelled subscription, by making a
		// REST call to AppDirect
		Subscription subscriptionCancel = SubscriptionClient.getSubscription(eventUrl);

		// Perform the necessary operation in my application using the
		// subscription information
		return subscriptionBusinessService.processCancelSubscription(subscriptionCancel);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.demo.xyzapp.service.ISubscriptionService#changeSubscription(java.
	 * lang.String)
	 */
	public NotificationResponse changeSubscription(String eventUrl) {

		// Get more information about the changed subscription, by making a REST
		// call to AppDirect
		Subscription subscriptionChange = SubscriptionClient.getSubscription(eventUrl);

		// Perform the necessary operation in my application using the
		// subscription information
		return subscriptionBusinessService.processChangeSubscription(subscriptionChange);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.demo.xyzapp.service.ISubscriptionService#subscriptionStatusChanged
	 * (java.lang.String)
	 */
	public NotificationResponse subscriptionStatusChanged(String eventUrl) {

		// Get more information about the subscription status change, by making
		// a REST call to AppDirect
		Subscription subscriptionNotice = SubscriptionClient.getSubscription(eventUrl);

		// Perform the necessary operation in my application using the
		// subscription information
		return subscriptionBusinessService.processSubscriptionStatusChange(subscriptionNotice);
	}

}
