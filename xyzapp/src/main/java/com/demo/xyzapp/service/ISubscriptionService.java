package com.demo.xyzapp.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.demo.xyzapp.model.NotificationResponse;

/**
 * This interface defines the endpoints which will serve the subscription
 * requests from AppDirect
 * 
 * @author Deepa
 *
 */
@Path("/subscription")
public interface ISubscriptionService {

	/**
	 * Invoked when a new subscription event occurs on AppDirect
	 * 
	 * @param eventUrl
	 *            The URI for fetching more information abouth the event
	 * @return NotificationResponse
	 */
	@GET
	@Path("/create")
	@Produces("application/json")
	NotificationResponse createSubscription(@QueryParam("eventurl") String eventUrl);

	/**
	 * Invoked when the subscription is cancelled on AppDirect
	 * 
	 * @param eventUrl
	 *            The URI for fetching more information abouth the event
	 * @return NotificationResponse
	 */
	@GET
	@Path("/cancel")
	@Produces("application/json")
	NotificationResponse cancelSubscription(@QueryParam("eventurl") String eventUrl);

	/**
	 * Invoked when a subscription is changed on AppDirect
	 * 
	 * @param eventUrl
	 *            The URI for fetching more information abouth the event
	 * @return NotificationResponse
	 */
	@GET
	@Path("/change")
	@Produces("application/json")
	NotificationResponse changeSubscription(@QueryParam("eventurl") String eventUrl);

	/**
	 * Invoked when a subscription status change occurs on AppDirect
	 * 
	 * @param eventUrl
	 *            The URI for fetching more information abouth the event
	 * @return NotificationResponse
	 */
	@GET
	@Path("/status")
	@Produces("application/json")
	NotificationResponse subscriptionStatusChanged(@QueryParam("eventurl") String eventUrl);
}
