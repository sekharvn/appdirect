package com.demo.xyzapp.service;

import java.util.List;

import com.demo.xyzapp.business.ContactBusinessService;
import com.demo.xyzapp.business.IContactBusinessService;
import com.demo.xyzapp.configuration.AppContext;
import com.demo.xyzapp.model.Contact;

/**
 * Contact Service implementation
 * 
 * @author Sekhar
 *
 */
public class ContactService implements IContactService {

	private IContactBusinessService contactBusinessService = AppContext.getAppContext().getBean(
			ContactBusinessService.class);

	/* (non-Javadoc)
	 * @see com.demo.xyzapp.service.IContactService#getAllContacts()
	 */
	public List<Contact> getAllContacts() {
		return contactBusinessService.getAllContacts();
	}

	/* (non-Javadoc)
	 * @see com.demo.xyzapp.service.IContactService#getContactByName(java.lang.String)
	 */
	public Contact getContactByName(String name) {
		return contactBusinessService.getContactByName(name);
	}

	/* (non-Javadoc)
	 * @see com.demo.xyzapp.service.IContactService#deleteContact(java.lang.String)
	 */
	public boolean deleteContact(String name) {
		return contactBusinessService.deleteContact(name);
	}

	/* (non-Javadoc)
	 * @see com.demo.xyzapp.service.IContactService#updateContact(com.demo.xyzapp.model.Contact)
	 */
	public Contact updateContact(Contact contact) {
		return contactBusinessService.updateContact(contact);
	}

	/* (non-Javadoc)
	 * @see com.demo.xyzapp.service.IContactService#createContact(com.demo.xyzapp.model.Contact)
	 */
	public Contact createContact(Contact contact) {
		return contactBusinessService.createContact(contact);
	}
}
