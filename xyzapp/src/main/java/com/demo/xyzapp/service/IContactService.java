package com.demo.xyzapp.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.demo.xyzapp.model.Contact;

/**
 * This interface defines the endpoints which will serve the Contact Services.
 * It supports all the CRUD operations, GET, POST, PUT and DELETE.
 * 
 * @author Sekhar
 *
 */
@Path("/Contacts")
public interface IContactService {

	/**
	 * Returns all the contacts
	 * 
	 * @return List of Contacts
	 */
	@GET
	@Produces("application/json")
	List<Contact> getAllContacts();

	/**
	 * Returns a particular contact by name
	 * 
	 * @param name
	 *            Name of Contact
	 * @return Contact
	 */
	@GET
	@Produces("application/json")
	@Path("/{name}")
	Contact getContactByName(@PathParam("name") String name);

	/**
	 * Updated the existing Contact
	 * 
	 * @param contact
	 *            Contact object you want to update
	 * @return Contact
	 */
	@PUT
	@Consumes("application/json")
	@Produces("application/json")
	Contact updateContact(Contact contact);

	/**
	 * Deletes the Contact
	 * 
	 * @param name
	 *            Name of Contact to be deleted
	 * @return true if Contact deleted, else will return false
	 */
	@DELETE
	@Produces("application/json")
	@Path("/{name}")
	boolean deleteContact(@PathParam("name") String name);

	/**
	 * Creates a new contact
	 * 
	 * @param contact
	 *            Contact to be created
	 * @return Contact Object which is created
	 */
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	Contact createContact(Contact contact);
}
