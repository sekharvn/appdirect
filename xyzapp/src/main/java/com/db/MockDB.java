package com.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.demo.xyzapp.model.Contact;

/**
 * This class mocks a database.
 * 
 * @author Sekhar
 *
 */
public class MockDB {

	private static MockDB mockDB;

	private static Map<String, Contact> contactsMap = new HashMap<String, Contact>();

	private MockDB() {

	}

	public static MockDB getInstance() {
		if (mockDB == null) {
			mockDB = new MockDB();
		}
		return mockDB;
	}

	public Contact insertContact(Contact contact) {
		contactsMap.put(contact.getName(), contact);
		return contact;
	}

	public Contact getContactByName(String name) {
		return contactsMap.get(name);
	}

	public List<Contact> getAllContacts() {
		return new ArrayList<Contact>(contactsMap.values());
	}

	public boolean deleteContact(String name) {
		contactsMap.remove(name);
		return true;
	}

	public Contact updateContact(Contact contact) {
		contactsMap.put(contact.getName(), contact);
		return contact;
	}
}
